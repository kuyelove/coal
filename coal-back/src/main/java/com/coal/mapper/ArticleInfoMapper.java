package com.coal.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coal.vo.ArticleInfo;

import java.util.List;
import java.util.Map;

public interface ArticleInfoMapper extends BaseMapper<ArticleInfo> {

     List<Map<String, Object>> mulSelect(ArticleInfo param);

     Integer countMulSelect(ArticleInfo param);
}
