package com.coal.service.impl;

import com.coal.pojo.UserRole;
import com.coal.mapper.UserRoleMapper;
import com.coal.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
