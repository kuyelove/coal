import {createStore} from 'vuex'

const store = createStore({
    state: {
        role: 1,
    },
    getters:{

    },
    //同步
    mutations:{

    },
    //同步或异步
    actions:{

    },
})

export default store