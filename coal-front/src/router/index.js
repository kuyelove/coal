import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'


import Login from '../views/Login.vue'
import Main from '../views/Main.vue'
import NotFound from '../views/NotFound.vue'
import Edit from "../views/Edit.vue";
import ArticlePage from "../views/ArticlePage.vue";
import ArticleList from "../views/ArticleList.vue";
import Fix from "../views/Fix.vue";

const router = createRouter({
    history: createWebHashHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
    routes: [
        {
          path:'/',
          redirect:'/login',
        },
        //登入页
        {
            path: '/login',
            name: 'Login',
            component: Login
        },

        //首页
        {
            path: '/main',
            name: 'Main',
            component: Main,
            children: [
                //编辑新资讯
                {
                    path: '/new/article',
                    name: 'Edit',
                    component: Edit,
                },

                //所有的展示list
                {
                    path: '/list/:statusId',
                    name: 'ArticleList',
                    component: ArticleList,
                },
                //资讯页
                {
                    path: '/ArticlePage/:articleId',
                    name: 'ArticlePage',
                    component: ArticlePage,
                },
                //修改资讯页
                {
                    path: '/fix/:articleId',
                    name: 'Fix',
                    component: Fix,
                },
            ]
        },

        //404
        {
            path: '/:pathMatch(.*)',
            component: NotFound
        }
    ]
})

router.beforeEach((to, from, next) => {
    // console.log(to);
    // console.log(from);
    // console.log(JSON.parse(window.sessionStorage.getItem('user')));
    if(JSON.parse(window.sessionStorage.getItem('user')) != null) {
        // console.log(to.meta.index);

        next();
    }else {
       if(to.path === '/login') {
           next();
       }
       else {
           next({path: '/login'});
       }
    }


})

export default router