package com.coal.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.coal.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(tags = "测试模块")
@RestController
public class Hello {

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

    @GetMapping("/json")
    public Object json() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "kuye");
        jsonObject.put("age", 18);
        return jsonObject;
    }

    @GetMapping("/json1")
    public Object json1() {
        JSONObject jsonObject = new JSONObject();
        User user = new User();
        user.setUsername("kuye");
        user.setPassword("kk");
        return JSON.toJSONString(user);
    }
}
