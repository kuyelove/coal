package com.coal.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.coal.pojo.User;
import com.coal.pojo.UserRole;
import com.coal.service.UserRoleService;
import com.coal.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Wrapper;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @ApiOperation(value="登入",notes="")
    @PostMapping("/login")
    public Object login(@RequestBody User user){
        JSONObject jsonObject = new JSONObject();
        System.out.println(user);
        User one = userService.getOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, user.getUsername()), false);

        if(one != null && one.getPassword().equals(user.getPassword())) {
            jsonObject.put("code", 1);
            UserRole one1 = userRoleService.getOne(Wrappers.<UserRole>lambdaQuery().eq(UserRole::getUserId, one.getUserId()), false);
            jsonObject.put("role", one1.getRoleId());
            jsonObject.put("userId", one.getUserId());
            jsonObject.put("username", one.getUsername());
            jsonObject.put("msg", "登入成功!");
        }
        else {
            jsonObject.put("code", 2);
            jsonObject.put("msg", "账户或密码错误!");
        }
        return jsonObject;
    }



}
