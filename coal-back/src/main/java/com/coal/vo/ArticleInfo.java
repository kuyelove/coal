package com.coal.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="ArticleInfo对象", description="")
public class ArticleInfo extends Model {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分页目前所在页")
    private Integer pageCurrent;

    @ApiModelProperty(value = "分页大小")
    private Integer pageSize;

    @ApiModelProperty(value = "资讯状态id")
    private Integer articleStatusId;

    @ApiModelProperty(value = "资讯id")
    private Integer articleId;

    @ApiModelProperty(value = "状态id")
    private Integer statusId;

    @ApiModelProperty(value = "发布资讯用户的id")
    private Integer userId;

    @ApiModelProperty(value = "资讯标题")
    private String articleTitle;

    @ApiModelProperty(value = "资讯文章内容")
    private String articleContent;

    @ApiModelProperty(value = "提交审核时间")
    private LocalDateTime submitTime;

    @ApiModelProperty(value = "资讯发布时间")
    private LocalDateTime releaseTime;


}
