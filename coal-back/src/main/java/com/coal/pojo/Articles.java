package com.coal.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="Articles对象", description="")
public class Articles extends Model {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "资讯id")
    @TableId(value = "article_id", type = IdType.AUTO)
    private Integer articleId;

    @ApiModelProperty(value = "发布资讯用户的id")
    private Integer userId;

    @ApiModelProperty(value = "资讯标题")
    private String articleTitle;

    @ApiModelProperty(value = "资讯文章内容")
    private String articleContent;

    @ApiModelProperty(value = "提交审核时间")
    private LocalDateTime submitTime;

    @ApiModelProperty(value = "资讯发布时间")
    private LocalDateTime releaseTime;


}
