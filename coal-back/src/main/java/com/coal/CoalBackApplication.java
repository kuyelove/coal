package com.coal;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.coal.mapper")
public class CoalBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoalBackApplication.class, args);
    }

}
