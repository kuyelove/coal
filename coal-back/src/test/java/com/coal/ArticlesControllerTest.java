package com.coal;

import com.coal.controller.ArticlesController;
import com.coal.pojo.Articles;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ArticlesControllerTest {
    @Test
    void insert() {
        ArticlesController articlesController =new ArticlesController();
        Articles articles =new Articles();
        articles.setUserId(123);
        articles.setArticleContent("五一没有休假，别想了，给爷干活！");
        articles.setArticleTitle("关于五一休假的通知！");
        articles.setArticleId(1234567);
        articlesController.editArticles(articles);
    }

}
