package com.coal.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.coal.pojo.ArticleStatus;
import com.coal.pojo.Articles;
import com.coal.pojo.User;
import com.coal.service.ArticleStatusService;
import com.coal.service.ArticlesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Action;
import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@RestController
@RequestMapping("/upStatus")
public class ArticleStatusController {

    @Autowired
    private ArticleStatusService articleStatusService;

    @Autowired
    private ArticlesService articlesService;




    @ApiOperation(value="把文章状态由等待审核改为不通过 1->2",notes="")
    @PutMapping("/toFail")
    public Object toFail(Integer articleId) {
        JSONObject jsonObject = new JSONObject();
        ArticleStatus one = articleStatusService.getOne(Wrappers.<ArticleStatus>lambdaQuery().eq(ArticleStatus::getArticleId, articleId), false);
        if(one != null && one.getStatusId() == 1) {
            boolean articleId1 = articleStatusService.update(new ArticleStatus().setStatusId(2), new UpdateWrapper<ArticleStatus>().eq("article_id", articleId));
            if(articleId1) {
                jsonObject.put("code", 1);
                jsonObject.put("msg", "资讯审核不通过成功!");
                return jsonObject;
            }
        }
        jsonObject.put("code", 2);
        jsonObject.put("msg", "资讯审核不通过失败!");
        return jsonObject;
    }

    @ApiOperation(value="把文章状态由不通过改为等待审核 2->1",notes="")
    @PutMapping("/examine")
    public Object toExamine(Integer articleId) {
        JSONObject jsonObject = new JSONObject();
        ArticleStatus one = articleStatusService.getOne(Wrappers.<ArticleStatus>lambdaQuery().eq(ArticleStatus::getArticleId, articleId), false);
        if(one != null && one.getStatusId() == 2) {
            boolean articleId1 = articleStatusService.update(new ArticleStatus().setStatusId(1), new UpdateWrapper<ArticleStatus>().eq("article_id", articleId));
            if(articleId1) {
                jsonObject.put("code", 1);
                jsonObject.put("msg", "重新提交审核成功!");
                return jsonObject;
            }
        }
        jsonObject.put("code", 2);
        jsonObject.put("msg", "重新提交审核失败!");
        return jsonObject;
    }


    @ApiOperation(value="把文章状态由等待审核改为通过即等待发布 1->3",notes="")
    @PutMapping("/toAdopt")
    public Object toAdopt(Integer articleId) {
        JSONObject jsonObject = new JSONObject();
        ArticleStatus one = articleStatusService.getOne(Wrappers.<ArticleStatus>lambdaQuery().eq(ArticleStatus::getArticleId, articleId), false);
        if(one != null && one.getStatusId() == 1) {
            boolean articleId1 = articleStatusService.update(new ArticleStatus().setStatusId(3), new UpdateWrapper<ArticleStatus>().eq("article_id", articleId));
            if(articleId1) {
                jsonObject.put("code", 1);
                jsonObject.put("msg", "资讯审核通过成功!");
                return jsonObject;
            }
        }
        jsonObject.put("code", 2);
        jsonObject.put("msg", "资讯审核通过失败!");
        return jsonObject;
    }


    @ApiOperation(value="把文章状态由等待发布改为已发布 3->4",notes="")
    @PutMapping("/release")
    public Object release(Integer articleId) {
        JSONObject jsonObject = new JSONObject();
        ArticleStatus one = articleStatusService.getOne(Wrappers.<ArticleStatus>lambdaQuery().eq(ArticleStatus::getArticleId, articleId), false);
        if(one != null && one.getStatusId() == 3) {
            boolean articleId1 = articleStatusService.update(new ArticleStatus().setStatusId(4), new UpdateWrapper<ArticleStatus>().eq("article_id", articleId));

            boolean article_id = articlesService.update(new Articles().setReleaseTime(LocalDateTime.now()), new UpdateWrapper<Articles>().eq("article_id", articleId));

            if(articleId1 && article_id) {
                jsonObject.put("code", 1);
                jsonObject.put("msg", "资讯发布成功!");
                return jsonObject;
            }
        }
        jsonObject.put("code", 2);
        jsonObject.put("msg", "资讯发布失败!");
        return jsonObject;
    }


    @ApiOperation(value="把文章状态由发布到撤回 4->3",notes="")
    @PutMapping("/withdraw")
    public Object withdraw(Integer articleId) {
        JSONObject jsonObject = new JSONObject();
        ArticleStatus one = articleStatusService.getOne(Wrappers.<ArticleStatus>lambdaQuery().eq(ArticleStatus::getArticleId, articleId), false);
        if(one != null && one.getStatusId() == 4) {
            boolean articleId1 = articleStatusService.update(new ArticleStatus().setStatusId(3), new UpdateWrapper<ArticleStatus>().eq("article_id", articleId));
            if(articleId1) {
                jsonObject.put("code", 1);
                jsonObject.put("msg", "资讯撤回成功!");
                return jsonObject;
            }
        }
        jsonObject.put("code", 2);
        jsonObject.put("msg", "资讯撤回失败!");
        return jsonObject;
    }
}
