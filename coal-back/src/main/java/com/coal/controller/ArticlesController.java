package com.coal.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coal.mapper.ArticlesMapper;
import com.coal.service.ArticleInfoService;
import com.coal.service.ArticleStatusService;
import com.coal.service.ArticlesService;

import com.coal.service.impl.ArticleStatusServiceImpl;
import com.coal.service.impl.ArticlesServiceImpl;
import com.coal.vo.ArticleInfo;
import com.coal.vo.ArticleInfoResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.coal.pojo.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@RestController
@RequestMapping("/articles")
public class ArticlesController {

    @Autowired
    private ArticlesService articlesService;

    @Autowired
    private ArticleStatusService articleStatusService;

    @Autowired
    private ArticleInfoService articleInfoService;

    @ApiOperation(value="编辑一篇新资讯",notes="")
    @PostMapping("/editArticles")
    public Object editArticles(@RequestBody Articles articles) {
        JSONObject jsonObject = new JSONObject();

        //获取提交的时间
        articles.setSubmitTime(LocalDateTime.now());

        //保存数据
        boolean save1 = articlesService.save(articles);
        System.out.println("================================");
        System.out.println(articles);

        if(save1) {
            //资讯状态为审核
            ArticleStatus articleStatus = new ArticleStatus();
            articleStatus.setArticleId(articles.getArticleId());
            articleStatus.setStatusId(1);
            boolean save = articleStatusService.save(articleStatus);

            if(save) {
                jsonObject.put("code", 1);
                jsonObject.put("msg", "资讯提交成功!");
                return jsonObject;
            }
            else {
                articlesService.removeById(articles.getArticleId());
            }
        }

        jsonObject.put("code", 2);
        jsonObject.put("msg", "资讯提交失败!");
        return jsonObject;
    }

    @ApiOperation(value="更新资讯",notes="")
    @PostMapping("/update")
    public Object updateArticles(@RequestBody Articles articles) {
        JSONObject jsonObject = new JSONObject();

        //获取提交的时间
        articles.setSubmitTime(LocalDateTime.now());

        //更新数据
        boolean save = articlesService.updateById(articles);

        if(save) {
            //资讯状态为审核
            jsonObject.put("code", 1);
            jsonObject.put("msg", "资讯更新成功!");
        }
        else {
            jsonObject.put("code", 2);
            jsonObject.put("msg", "资讯更新失败!");
        }
        return jsonObject;

    }

    //搜索
//    @GetMapping("/search")
//    public Object search(String title){
//        Page<Articles> page = new Page(1, 5);
//        QueryWrapper<Articles> wrapper = new QueryWrapper<>();
//        wrapper.like("article_title", title);
//        articlesService.page(page, wrapper);
//        List<Articles> records = page.getRecords();
//        return JSON.toJSONString(records);
//    }
//

    @ApiOperation(value="分页查询",notes="每页大小固定为6")
    @GetMapping("/list")
    public Object list(Integer pageCurrent, Integer statusId, Integer userId){

        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setPageSize(6);
        articleInfo.setPageCurrent((pageCurrent - 1) * articleInfo.getPageSize());
        articleInfo.setStatusId(statusId);

        articleInfo.setUserId(userId);

        ArticleInfoResult articleInfoResult = articleInfoService.mulSelect(articleInfo);

        return JSON.toJSONString(articleInfoResult);
    }


    @ApiOperation(value="根据id来查询文章",notes="")
    @GetMapping("/queryById")
    public Object queryById(Integer id){
        JSONObject jsonObject = new JSONObject();
        Articles one = articlesService.getById(id);

        ArticleStatus byId = articleStatusService.getOne(Wrappers.<ArticleStatus>lambdaQuery().eq(ArticleStatus::getArticleId, id), false);
        jsonObject = (JSONObject)JSON.parse(JSON.toJSONString(one));

        jsonObject.put("statusId",byId.getStatusId());
        return jsonObject;
    }
}
