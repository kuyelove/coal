package com.coal.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@ApiModel(value="分页查询反回对象", description="")
public class ArticleInfoResult {
    private List<Map<String, Object>> data;
    private Integer pageNo;
    private Integer total;
    private Integer pageSize;
}
