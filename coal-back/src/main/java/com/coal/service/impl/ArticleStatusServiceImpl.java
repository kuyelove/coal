package com.coal.service.impl;

import com.coal.pojo.ArticleStatus;
import com.coal.mapper.ArticleStatusMapper;
import com.coal.service.ArticleStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@Service
public class ArticleStatusServiceImpl extends ServiceImpl<ArticleStatusMapper, ArticleStatus> implements ArticleStatusService {

}
