package com.coal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coal.vo.ArticleInfo;
import com.coal.vo.ArticleInfoResult;

public interface ArticleInfoService extends IService<ArticleInfo> {

    ArticleInfoResult mulSelect(ArticleInfo param);

}
