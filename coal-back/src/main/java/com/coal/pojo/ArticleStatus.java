package com.coal.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="ArticleStatus对象", description="")
public class ArticleStatus extends Model {

    private static final long serialVersionUID = 1L;

    @TableId(value = "article_status_id", type = IdType.AUTO)
    private Integer articleStatusId;

    @ApiModelProperty(value = "资讯id")
    private Integer articleId;

    @ApiModelProperty(value = "状态id")
    private Integer statusId;


}
