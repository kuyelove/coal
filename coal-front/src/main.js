import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import router from './router/index.js'
import axios from "./utils/http";
import store from "./store/index.js";

const app = createApp(App)
app.config.globalProperties.$axios = axios
app.use(store)
app.use(ElementPlus)
app.use(router)
app.mount('#app')
