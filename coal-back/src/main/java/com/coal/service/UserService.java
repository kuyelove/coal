package com.coal.service;

import com.coal.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
public interface UserService extends IService<User> {

}
