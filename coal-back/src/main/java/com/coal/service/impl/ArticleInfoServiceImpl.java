package com.coal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coal.mapper.ArticleInfoMapper;
import com.coal.mapper.ArticlesMapper;
import com.coal.pojo.Articles;
import com.coal.service.ArticleInfoService;
import com.coal.service.ArticlesService;
import com.coal.vo.ArticleInfo;
import com.coal.vo.ArticleInfoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ArticleInfoServiceImpl extends ServiceImpl<ArticleInfoMapper, ArticleInfo> implements ArticleInfoService{

    @Autowired
    private ArticleInfoMapper articleInfoMapper;

    @Override
    public ArticleInfoResult mulSelect(ArticleInfo param) {
        ArticleInfoResult articleInfoResult = new ArticleInfoResult();
        List<Map<String, Object>> maps = articleInfoMapper.mulSelect(param);
        articleInfoResult.setData(maps);
        Integer integer = articleInfoMapper.countMulSelect(param);
        articleInfoResult.setTotal(integer);
        articleInfoResult.setPageSize(param.getPageSize());

        return articleInfoResult;
    }
}
