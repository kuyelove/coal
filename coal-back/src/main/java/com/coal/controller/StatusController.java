package com.coal.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
@RestController
@RequestMapping("/status")
public class StatusController {

}
