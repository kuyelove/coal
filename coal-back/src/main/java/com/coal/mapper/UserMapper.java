package com.coal.mapper;

import com.coal.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
public interface UserMapper extends BaseMapper<User> {

}
