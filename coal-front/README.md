**初始化**：

- npm init vite-app vite_test
- cd vite_test
- npm install

**调试：**

- npm run dev

**发布：**

- npm run build

如果是yarn，则相应的命令是

- yarn create vite-app vite_test
- cd vite_test
- yarn
- yarn dev