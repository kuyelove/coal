package com.coal.mapper;

import com.coal.pojo.Status;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kuye
 * @since 2021-07-26
 */
public interface StatusMapper extends BaseMapper<Status> {

}
